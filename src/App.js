import { ROUTE_HOME, ROUTE_LOGIN, ROUTE_FILEUPLOAD, ROUTE_REPORTS, ROUTE_REGISTER, ROUTE_STATUSES, ROUTE_TEMPLATEFILES, ROUTE_FILES } from "./constants/routeConstants"
import { NavigationBar, Login, Register, Home, FileUpload, Reports, Alert, StatusList, TemplateFiles, Files } from "./components"
import "bootswatch/dist/journal/bootstrap.min.css"
import { useSelector } from "react-redux"

const getRouteComponent = (route) => {
  switch (route) {
    case ROUTE_HOME:
      return <Home />
    case ROUTE_LOGIN:
      return <Login />
    case ROUTE_REGISTER:
      return <Register />
    case ROUTE_FILEUPLOAD:
      return <FileUpload />
    case ROUTE_REPORTS:
      return <Reports />
    case ROUTE_STATUSES:
      return <StatusList />
    case ROUTE_TEMPLATEFILES:
      return <TemplateFiles />
    case ROUTE_FILES:
      return <Files />
    default:
      return <Login />
  }
}


const App = () => {
  const route = useSelector((state) => state.routing.route)

  return (
    <div className="container-fluid px-0">
      <NavigationBar />
      <Alert />
      <div>
        {getRouteComponent(route)}</div>
    </div>
  )
}

export default App

