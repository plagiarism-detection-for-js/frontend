import TemplateFile from "./base/TemplateFile"
import { useDispatch, useSelector } from "react-redux"
import { getTemplateFiles } from "../actions"
import { useEffect } from "react"


const TemplateFiles = () => {
    const authToken = useSelector((state) => state.auth.authToken)
    const templatesList = useSelector((state) => state.file.templatesData)
    const dispatch = useDispatch();

    useEffect(() => {
        getTemplateFiles(dispatch, authToken);
    }, []);

    let templates = <div />

    if (templatesList !== "") {
        templates = templatesList.map((templateFile, id) => (
            <TemplateFile templateFile={templateFile} key={id} />
        ))
    }

    return (
        <div className="container col-8 mt-4">
            <div className="jumbotron pt-4">
                <div className="form-group">
                    <h1>Template files</h1>
                    <p className="lead mt-4 mb-5">Below you can find the list of files registered in the system as template files.</p>
                    <div className="container py-2" style={{ fontWeight: 600, fontSize: 17 }}>
                        <div className="row align-items-center d-flex">
                            <div className="col-2 d-flex justify-content-center" style={{ fontWeight: 600 }}>
                                FILE ID
                            </div>
                            <div className="col-4 d-flex justify-content-start">
                                FILE NAME
                            </div>
                        </div>
                    </div >
                    {templates}
                </div>
            </div>
        </div>
    )
}

export default TemplateFiles
