import { useDispatch, useSelector } from "react-redux"
import { getStatuses } from "../actions"
import { useEffect } from "react"
import Status from "./base/Status"


const StatusList = () => {
    const authToken = useSelector((state) => state.auth.authToken)
    const statusesList = useSelector((state) => state.analysis.statusesData)
    const dispatch = useDispatch();

    useEffect(() => {
        getStatuses(dispatch, authToken);
    }, []);


    let statuses = <div />

    console.log(statusesList)
    if (statusesList !== "") {
        statuses = statusesList.map((status, id) => (
            <Status status={status} key={id} />
        ))
    }

    return (
        <div className="container col-8 mt-4">
            <div className="jumbotron pt-4">
                <div className="form-group">
                    <h1>Status of the analysis</h1>
                    <p className="lead mt-4 mb-5">Below you can find the status of all analyses.</p>
                    <div className="container py-2" style={{ fontWeight: 600, fontSize: 17 }}>
                        <div className="row align-items-center">
                            <div className="col-2 d-flex justify-content-center" style={{ fontWeight: 600 }}>
                                ANALYSIS ID
                            </div>
                            <div className="col d-flex justify-content-center">
                                FILE ID
                            </div>
                            <div className="col d-flex justify-content-center">
                                FILE NAME
                            </div>
                            <div className="col d-flex justify-content-center">
                                STATUS
                            </div>
                            <div className="col-2 d-flex justify-content-center">
                            </div>
                        </div>
                    </div >
                    {statuses}
                </div>
            </div>
        </div>
    )
}

export default StatusList
