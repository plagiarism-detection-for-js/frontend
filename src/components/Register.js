import { Form, InputGroup, Button } from "react-bootstrap"
import { ROUTE_LOGIN } from "../constants/routeConstants"
import { useDispatch } from "react-redux"
import { register, switchRoute } from "../actions"
import { useState } from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faEye } from "@fortawesome/free-solid-svg-icons"
import "../style.css"

const eye = <FontAwesomeIcon icon={faEye} />

const Register = () => {
    const [passwordHidden, setPasswordHidden] = useState(true)
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const dispatch = useDispatch()

    const validateForm = () => {
        return username.length > 0 && password.length > 0
    }

    const handleSubmit = (event) => {
        event.preventDefault()
    }

    return (
        <div className="card border-primary col-4 p-0 mt-5 mx-auto">
            <div
                className="card-header"
                style={{ textAlign: "center", fontWeight: 700 }}
            >
                Sign up
      </div>
            <Form className="card-body px-4 pb-1" onSubmit={(e) => handleSubmit(e)}>
                <Form.Group controlId="username">
                    <Form.Label>Username</Form.Label>
                    <Form.Control
                        autoFocus
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                    />
                </Form.Group>
                <Form.Group className="pb-1" size="lg" controlId="password">
                    <Form.Label>Password</Form.Label>
                    <InputGroup className="mb-2">
                        <Form.Control
                            type={passwordHidden ? "password" : ""}
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                        <InputGroup.Append>
                            <InputGroup.Text
                                onClick={() => setPasswordHidden(!passwordHidden)}
                            >
                                {passwordHidden ? (
                                    <div className="image">{eye}</div>
                                ) : (
                                    <div className="hidden-image">{eye}</div>
                                )}
                            </InputGroup.Text>
                        </InputGroup.Append>
                    </InputGroup>
                </Form.Group>
                <div className="row m-0 mb-5 ">
                    <small>Return back to </small>
                    <div
                        className="my-link pl-1"
                        onClick={() => switchRoute(dispatch, ROUTE_LOGIN)}
                    >
                        Log in page
                </div>
                </div>
                <Button
                    block
                    size="lg"
                    type="submit"
                    className="mb-3"
                    disabled={!validateForm()}
                    onClick={() => register(dispatch, username, password)}
                >
                    Register
        </Button>
            </Form>
        </div>
    )
}

export default Register
