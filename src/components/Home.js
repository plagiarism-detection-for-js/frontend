import { ROUTE_FILEUPLOAD } from "../constants/routeConstants"
import { useDispatch } from "react-redux"
import { switchRoute } from "../actions"


const Home = () => {
    const dispatch = useDispatch()

    return (
        <div className="container col-8 mt-4">
            <div className="jumbotron">
                <h1 className="display-3">Hello!</h1>
                <p className="lead my-4">
                    This is a tool that enables the detection of plagiarism in JavaScript source code.
                </p>
                <hr className="pt-4" />
                <p className="lead my-3">Upload your file for analysis here:</p>
                <p className="lead pt-3">
                    <div
                        className="btn btn-primary btn-lg "
                        onClick={() => switchRoute(dispatch, ROUTE_FILEUPLOAD)}
                        role="button"
                    >
                        Upload file
                    </div>
                </p>
            </div>
        </div>
    )
}

export default Home
