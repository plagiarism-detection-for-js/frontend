import { useDispatch, useSelector } from "react-redux"
import { getFiles } from "../actions"
import { useEffect } from "react"
import File from "./base/File"


const Files = () => {
    const authToken = useSelector((state) => state.auth.authToken)
    const filesList = useSelector((state) => state.file.filesData)
    const dispatch = useDispatch();

    useEffect(() => {
        getFiles(dispatch, authToken);
    }, []);


    let files = <div />

    if (filesList !== "") {
        files = filesList.map((file, id) => (
            <File file={file} key={id} />
        ))
    }

    return (
        <div className="container col-8 mt-4">
            <div className="jumbotron pt-4">
                <div className="form-group">
                    <h1>Uploaded files</h1>
                    <p className="lead mt-4 mb-5">Below you can find the list of uploaded files.</p>
                    <div className="container py-2" style={{ fontWeight: 600, fontSize: 17 }}>
                        <div className="row align-items-center d-flex">
                            <div className="col-2 d-flex justify-content-center" style={{ fontWeight: 600 }}>
                                FILE ID
                            </div>
                            <div className="col d-flex justify-content-left">
                                FILE NAME
                            </div>
                            <div className="col-3">
                            </div>
                            <div className="col-3">
                            </div>
                        </div>
                    </div >
                    {files}
                </div>
            </div>
        </div>
    )
}

export default Files
