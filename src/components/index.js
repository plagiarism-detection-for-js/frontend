import NavigationBar from "./base/NavigationBar"
import Login from "./Login"
import Home from "./Home"
import FileUpload from "./FileUpload"
import Reports from "./Reports"
import Register from "./Register"
import Alert from "./base/Alert"
import StatusList from "./StatusList"
import TemplateFiles from "./TemplateFiles"
import Files from "./Files"

export { NavigationBar, Register, Login, Home, FileUpload, Reports, Alert, StatusList, TemplateFiles, Files }
