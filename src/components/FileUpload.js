import { uploadFile, uploadTemplateFile, showAlert } from "../actions"
import { ERROR_ALERT } from "../constants/alertConstants"
import { useDispatch, useSelector } from "react-redux"
import { useState } from "react"


const FileUpload = () => {
    const authToken = useSelector((state) => state.auth.authToken)

    const [submitDisabled, setSubmitDisabled] = useState(true)
    const [templateFile, setTemplateFile] = useState(false)
    const [file, setFile] = useState("")

    const dispatch = useDispatch();


    const handleSubmit = e => {
        e.preventDefault()
        if (templateFile) {
            uploadTemplateFile(dispatch, authToken, file)
        }
        else {
            uploadFile(dispatch, authToken, file)
        }
    }

    const handleFileChanged = (e) => {
        e.preventDefault()

        if (!e.target.files[0]) {
            return
        }

        if (e.target.files[0].type === "text/javascript") {
            setSubmitDisabled(false)
            setFile(e.target.files[0])
        }
        else {
            showAlert(dispatch, ERROR_ALERT, "Only JavaScript (.js) files are allowed")
            document.getElementById("validatedCustomFile").value = ""
            setFile("")
            setSubmitDisabled(true)
        }
    }


    return (
        <div className="container col-8 mt-4">
            <div className="jumbotron py-4">
                <div className="form-group">
                    <h1>Upload file</h1>
                    <p className="lead my-4">Choose a file you want to upload. <br></br>If you want to upload a template file, check the box "Upload as template file".</p>
                    <div className="custom-file col-7 mt-4">
                        <input
                            type="file"
                            className="custom-file-input"
                            id="validatedCustomFile"
                            required
                            onChange={(e) => handleFileChanged(e)}
                            accept=".js"
                        />
                        <label className="custom-file-label pl-2" htmlFor="validatedCustomFile">
                            {file === "" ? "Choose file..." : file.name}
                        </label>
                    </div>
                    <small id="fileHelp" className="form-text text-muted pl-1 pt-1">
                        Accepted file type: .js
                    </small>
                    <div className="custom-control custom-checkbox mt-4">
                        <input
                            type="checkbox"
                            className="custom-control-input"
                            id="customCheck1"
                            onChange={() => setTemplateFile(!templateFile)}
                        />
                        <label className="custom-control-label my-2" htmlFor="customCheck1">
                            Upload as template file
                    </label>
                    </div>
                    <button
                        type="submit"
                        className="btn btn-primary mt-5"
                        disabled={submitDisabled}
                        onClick={(e) => handleSubmit(e)}
                    >
                        Upload file
                </button>
                </div>
            </div>
        </div>
    )
}

export default FileUpload
