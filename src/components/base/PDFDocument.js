import { Page, Text, View, Document, StyleSheet } from "@react-pdf/renderer"

const styles = StyleSheet.create({
    page: {
        flexDirection: "row",
        backgroundColor: "#FFFFFF",
    },
    section: {
        marginHorizontal: 20,
        marginVertical: 5,
        padding: 5,
        flexGrow: 1,
    },
    text_data: {
        marginHorizontal: 20,
        marginBottom: 15,
        padding: 5,
        flexGrow: 1,
        color: "#eb6864",
    },
    header: {
        width: 595,
        height: 60,
        "@media max-width: 500": {
            width: 300,
        },
        "@media orientation: portrait": {
            width: 500,
        },
        backgroundColor: "#eb6864",
    },
    header_text: {
        marginTop: 18,
        marginLeft: 15,
        color: "#FFFFFF",
        fontWeight: 500,
        fontSize: 20,
    },
    subheader_text: {
        marginHorizontal: 20,
        marginTop: 20,
        marginBottom: 40,
        padding: 5,
        fontWeight: 500,
        fontSize: 20,
    },
})


const PDFDocument = ({ reportData }) => {
    const parsedData = JSON.parse(reportData)
    let analysisDate = new Intl.DateTimeFormat("en-GB", {
        year: "numeric",
        month: "2-digit",
        day: "2-digit",
        hour: "2-digit",
        minute: "2-digit",
        second: "2-digit",
    }).format(parsedData.date)

    return (
        <Document>
            <Page size="A4" style={styles.page}>
                <View>
                    <View style={styles.header}>
                        <Text style={styles.header_text}>PLAGIARISM DETECTION</Text>
                    </View>
                    <View style={styles.section}>
                        <Text style={styles.subheader_text}>REPORT FROM ANALYSIS</Text>
                        <Text style={styles.section}>Analysis ID:</Text>
                        <Text style={styles.text_data}>{parsedData.analysisId}</Text>
                        <Text style={styles.section}>Result of the analysis:</Text>
                        <Text style={styles.text_data}>{`${parsedData.result}%`}</Text>
                        <Text style={styles.section}>Examined file name:</Text>
                        <Text style={styles.text_data}>{parsedData.fileName}</Text>
                        <Text style={styles.section}>Date of the analysis:</Text>
                        <Text style={styles.text_data}>{analysisDate}</Text>
                    </View>
                </View>
            </Page>
        </Document>
    )
}

export default PDFDocument
