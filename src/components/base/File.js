import { useDispatch, useSelector } from "react-redux"
import { sendToAnalysis, markAsTemplate } from "../../actions"



const File = ({ file }) => {
    const authToken = useSelector((state) => state.auth.authToken)
    const dispatch = useDispatch()

    console.log(file)


    return (
        <div className="container">
            <div className="row align-items-center  border-top border-secondary">
                <div className="col-2 d-flex justify-content-center" style={{ fontWeight: 600 }}>
                    {file.id}
                </div>
                <div className="col d-flex d-inline-block text-truncate text-left">
                    {file.name}
                </div>
                <div className="col-3 d-flex justify-content-center my-2" style={{ color: "#EB6864" }}>
                    <div
                        className="my-link pl-1 secondary"
                        style={{ textDecoration: "underline", fontSize: 14 }}
                        onClick={() => markAsTemplate(dispatch, authToken, file.id)}
                    >
                        Mark as template
                    </div>
                </div>
                <div className="col-3 d-flex justify-content-center my-2" style={{ color: "#EB6864" }}>
                    <button
                        type="button"
                        className="btn btn-outline-primary"
                        onClick={() => sendToAnalysis(dispatch, authToken, file.id)}
                    >
                        Send to analysis
                    </button>
                </div>
            </div>
        </div >
    )
}

export default File
