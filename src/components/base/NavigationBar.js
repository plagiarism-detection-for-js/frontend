
import { ROUTE_HOME, ROUTE_FILEUPLOAD, ROUTE_REPORTS, ROUTE_STATUSES, ROUTE_TEMPLATEFILES, ROUTE_FILES } from "../../constants/routeConstants"
import { useSelector, useDispatch } from "react-redux"
import { switchRoute, logout } from "../../actions"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faSignOutAlt } from "@fortawesome/free-solid-svg-icons"

const signOutIcon = <FontAwesomeIcon icon={faSignOutAlt} />

const NavigationBar = () => {
    const route = useSelector((state) => state.routing.route)
    const isSignedIn = useSelector((state) => state.auth.isSignedIn)
    const dispatch = useDispatch()


    return (
        <nav className="navbar navbar-expand-sm navbar-dark bg-primary my-auto">
            {isSignedIn ? (
                <div className="navbar-nav w-100">
                    <div
                        className="navbar-brand pr-4"
                        role="button"
                        style={{ fontSize: 20 }}
                        onClick={() => switchRoute(dispatch, ROUTE_HOME)}
                    >
                        PLAGIARISM DETECTION
                        </div>
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item mx-2">
                            <div
                                className={`${route === ROUTE_FILEUPLOAD ? "nav-link active" : "nav-link"
                                    }`}
                                role="button"
                                onClick={() => switchRoute(dispatch, ROUTE_FILEUPLOAD)}
                            >
                                Upload file
                            </div>
                        </li>
                        <li className="nav-item mx-2">
                            <div
                                className={`${route === ROUTE_STATUSES ? "nav-link active" : "nav-link"
                                    }`}
                                role="button"
                                onClick={() => switchRoute(dispatch, ROUTE_STATUSES)}
                            >
                                Status list
                            </div>
                        </li>
                        <li className="nav-item mx-2">
                            <div
                                className={`${route === ROUTE_REPORTS ? "nav-link active" : "nav-link"
                                    }`}
                                role="button"
                                onClick={() => switchRoute(dispatch, ROUTE_REPORTS)}
                            >
                                Reports
                            </div>
                        </li>
                        <li className="nav-item mx-2">
                            <div
                                className={`${route === ROUTE_FILES ? "nav-link active" : "nav-link"
                                    }`}
                                role="button"
                                onClick={() => switchRoute(dispatch, ROUTE_FILES)}
                            >
                                Files
                            </div>
                        </li>
                        <li className="nav-item mx-2">
                            <div
                                className={`${route === ROUTE_TEMPLATEFILES ? "nav-link active" : "nav-link"
                                    }`}
                                role="button"
                                onClick={() => switchRoute(dispatch, ROUTE_TEMPLATEFILES)}
                            >
                                Templates
                            </div>
                        </li>
                    </ul>
                    <div
                        type="button"
                        className="mr-4 my-auto"
                        // data-container="body"
                        data-toggle="popover"
                        data-placement="right"
                        style={{ color: "#FFFFFF", fontSize: 26 }}
                        onClick={() => logout(dispatch)}
                    >
                        {signOutIcon}
                    </div>
                </div>
            ) :
                (<div
                    className="navbar-brand pr-4"
                    style={{ fontSize: 20 }}
                >
                    PLAGIARISM DETECTION
                </div>
                )}
        </nav>
    )
}

export default NavigationBar
