import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faFilePdf } from "@fortawesome/free-solid-svg-icons"
import { PDFDownloadLink } from "@react-pdf/renderer";
import PDFDocument from "./PDFDocument"
import { useState } from "react"

const fileDownload = <FontAwesomeIcon icon={faFilePdf} />


const Report = ({ report }) => {
    const [buttonClicked, setButtonClicked] = useState(false)

    let parsedData = JSON.parse(report)


    const handleClick = (e) => {
        e.preventDefault()
        setButtonClicked(true)
    }

    return (
        <div className="container border-top border-secondary">
            <div className="row align-items-center my-2">
                <div className="col-2 d-flex justify-content-center" style={{ fontWeight: 600 }}>
                    {parsedData.analysisId}
                </div>
                <div className="col-2 d-flex justify-content-center" style={{ fontWeight: 600 }}>
                    {parsedData.fileId}
                </div>
                <div className="col d-flex justify-content-center">
                    {parsedData.fileName}
                </div>
                <div className="col-3 fa-2x d-flex justify-content-center" style={{ color: "#EB6864", height: 40 }}>
                    {buttonClicked ?
                        (<PDFDownloadLink
                            document={<PDFDocument reportData={report} />}
                            fileName={`Analysis${parsedData.analysisId}.pdf`}
                        >
                            {fileDownload}
                        </PDFDownloadLink>)
                        : (<button
                            type="button"
                            className="btn btn-outline-primary"
                            onClick={(e) => handleClick(e)}
                        // style={{ fontSize: 15 }}
                        >
                            Generate PDF
                        </button>)
                    }
                </div>
            </div>
        </div >
    )
}

export default Report
