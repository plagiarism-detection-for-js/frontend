import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faAngleDoubleRight, faCheckSquare, faSpinner } from "@fortawesome/free-solid-svg-icons"
import "../../style.css"

const doubleArrowIcon = <FontAwesomeIcon icon={faAngleDoubleRight} />
const checkIcon = <FontAwesomeIcon icon={faCheckSquare} />
const spinnerIcon = <FontAwesomeIcon icon={faSpinner} className="slow-spin" />


const Status = ({ status }) => {

    const statusIcon = () => {
        switch (status.status) {
            case "PENDING":
                return (
                    <div className="col-1 fa-2x fa-blink" style={{ color: "#EB6864" }}>
                        {doubleArrowIcon}
                    </div>
                )
            case "IN_PROGRESS":
                return (
                    <div className="col-1 fa-2x" style={{ color: "#EB6864" }}>
                        {spinnerIcon}
                    </div>)
            case "DONE":
                return (
                    <div className="col-1 fa-2x" style={{ color: "#EB6864" }}>
                        {checkIcon}
                    </div>)
            default:
                return (<div></div>)
        }
    }

    const statusText = () => {
        switch (status.status) {
            case "PENDING":
                return ("Pending")
            case "IN_PROGRESS":
                return ("In progress")
            case "DONE":
                return ("Done")
            default:
                return (<div></div>)
        }
    }

    return (

        <div className="container border-top border-secondary">
            <div className="row align-items-center">
                <div className="col-2 d-flex justify-content-center" style={{ fontWeight: 600 }}>
                    {status.id}
                </div>
                <div className="col d-flex justify-content-center">
                    {status.fileId}
                </div>
                <div className="col d-flex justify-content-center">
                    {status.fileName}
                </div>
                <div className="col d-flex justify-content-center">
                    {statusText()}
                </div>
                <div className="col-2 d-flex justify-content-center">
                    {statusIcon()}
                </div>
            </div>
        </div >
    )
}

export default Status
