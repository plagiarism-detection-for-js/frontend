
const TemplateFile = ({ templateFile }) => {
    console.log(templateFile)

    return (
        <div className="container border-top border-secondary">
            <div className="row align-items-center my-2">
                <div className="col-2 d-flex justify-content-center my-2" style={{ fontWeight: 600 }}>
                    {templateFile.id}
                </div>
                <div className="col-8 d-flex my-2 d-inline-block text-truncate text-left">
                    {templateFile.name}
                </div>
            </div>
        </div >
    )
}

export default TemplateFile
