import { NO_ALERT, SUCCESS_ALERT, WARNING_ALERT, ERROR_ALERT } from "../../constants/alertConstants"
import { useSelector } from "react-redux"


const Alert = () => {
    const alert = useSelector((state) => state.alert.alert)
    const message = useSelector((state) => state.alert.message)

    const getMessageHeader = () => {
        switch (alert) {
            case SUCCESS_ALERT:
                return "SUCCESS!"
            case WARNING_ALERT:
                return "WARNING!"
            case ERROR_ALERT:
                return "ERROR!"
            default:
                return ""
        }
    }

    const view = alert === NO_ALERT ?
        <div /> :
        <div className={`${alert} container col-11 my-3 py-1`}>
            <div className="row">
                <p className="lead my-auto px-4" style={{ fontWeight: 600 }}>{getMessageHeader()}</p>
                <p className="my-auto">{message}</p>
            </div>
        </div>


    return view
}

export default Alert
