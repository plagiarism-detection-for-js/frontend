import { useDispatch, useSelector } from "react-redux"
import { getReports } from "../actions"
import { useEffect } from "react"
import Report from "./base/Report"


const Reports = () => {
    const authToken = useSelector((state) => state.auth.authToken)
    const reportsList = useSelector((state) => state.analysis.reportsData)
    const dispatch = useDispatch();

    useEffect(() => {
        getReports(dispatch, authToken);
    }, []);


    let reports = <div />

    if (reportsList !== "") {
        reports = reportsList.map((report, id) => (
            <Report report={JSON.stringify(report)} key={id} />
        ))
    }

    return (
        <div className="container col-8 mt-4">
            <div className="jumbotron pt-4">
                <div className="form-group">
                    <h1>Reports from the analysis</h1>
                    <p className="lead mt-4 mb-5">Below you can find reports from the performed analyses.</p>
                    <div className="container py-2" style={{ fontWeight: 600, fontSize: 17 }}>
                        <div className="row align-items-center">
                            <div className="col-2 d-flex justify-content-center">
                                ANALYSIS ID
                        </div>
                            <div className="col-2 d-flex justify-content-center">
                                FILE ID
                        </div>
                            <div className="col d-flex justify-content-center">
                                FILE NAME
                        </div>
                            <div className="col-3 d-flex justify-content-center">
                            </div>
                        </div>
                    </div>
                    {reports}
                </div>
            </div>
        </div>
    )
}

export default Reports
