import { ERROR_ALERT, SUCCESS_ALERT } from "../constants/alertConstants"
import { UPDATE_TEMPLATES_LIST } from "../constants/fileConstants"
import { API_URL } from "../constants/APIConstants"
import { showAlert } from './alert'
import axios from "axios"

// TODO: remove this.
import { templates } from "./tmp"
import { getFiles } from "./file"

export const getTemplateFiles = async (dispatch, authToken) => {

    await axios
        .get(API_URL + "/download/template", {
            headers: {
                "Authorization": `Bearer ${authToken}`
            },
        })
        .then((response) => {
            if (response.data) {
                dispatch({
                    type: UPDATE_TEMPLATES_LIST,
                    payload: response.data
                })
                //TODO: Remove this
                showAlert(dispatch, SUCCESS_ALERT, "Successfully fetched the list of template files")
            }
        })
        .catch((error) => {
            // TODO: Remove this.
            dispatch({
                type: UPDATE_TEMPLATES_LIST,
                payload: templates
            })
            showAlert(dispatch, ERROR_ALERT, "Failed to fetch the list of template files")
        })
}


export const uploadTemplateFile = async (dispatch, authToken, templateFile) => {
    const formData = new FormData()
    formData.append("file", templateFile)

    await axios
        .post(API_URL + "/upload/template", formData, {
            headers: {
                "Content-Type": "multipart/form-data",
                "Authorization": `Bearer ${authToken}`
            },
        })
        .then((response) => {
            if (response.data) {
                showAlert(dispatch, SUCCESS_ALERT, "Template file has been uploaded")
            }
        })
        .catch((error) => {
            showAlert(dispatch, ERROR_ALERT, "Failed to upload template file")
        })
}



export const markAsTemplate = async (dispatch, authToken, fileId) => {

    console.log(authToken)

    await axios
        .patch(API_URL + `/upload/make-template/${fileId}`, {}, {
            headers: {
                "Authorization": `Bearer ${authToken}`
            },
        })
        .then((response) => {
            showAlert(dispatch, SUCCESS_ALERT, "File has been registered as template file")

            getFiles(dispatch, authToken)
        })
        .catch((error) => {
            showAlert(dispatch, ERROR_ALERT, "Failed to register new template file")
        })
}