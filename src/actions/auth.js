import { ERROR_ALERT, SUCCESS_ALERT } from "../constants/alertConstants"
import { ROUTE_HOME, ROUTE_LOGIN } from "../constants/routeConstants"
import { SIGN_IN, SIGN_OUT } from "../constants/authConstants"
import { API_URL } from "../constants/APIConstants"
import switchRoute from '../actions/route'
import { showAlert } from './alert'
import axios from "axios"


const handleLoginSuccess = (dispatch, data) => {
    switchRoute(dispatch, ROUTE_HOME)
    dispatch({
        type: SIGN_IN,
        payload: {
            authToken: data
        }
    })
}

export const login = async (dispatch, username, password) => {
    await axios.post(API_URL + "/auth/login", {
        username,
        password
    }).then((response => {
        console.log(response)
        if (response.status === 200) {
            console.log("Is OK")
            console.log(response.data)
            handleLoginSuccess(dispatch, response.data)
            showAlert(dispatch, SUCCESS_ALERT, "You are now logged in")
        }
    })
    ).catch(error => {
        showAlert(dispatch, ERROR_ALERT, "Failed to login")
    })
}

export const logout = (dispatch) => {
    dispatch({
        type: SIGN_OUT
    })
    switchRoute(dispatch, ROUTE_LOGIN)
}

export const register = async (dispatch, username, password) => {
    //TODO: remove this
    // showAlert(dispatch, SUCCESS_ALERT, "You are now registered and logged in")
    // return handleLoginSuccess(dispatch, "123")
    await axios.post(API_URL + "/auth/register", {
        username,
        password
    }).then((response => {
        if (response.data) {
            handleLoginSuccess(dispatch, response.data)
            showAlert(dispatch, SUCCESS_ALERT, "You are now registered and logged in")
        }
    })
    ).catch(error => {
        showAlert(dispatch, ERROR_ALERT, "Failed to register")
    })
}
