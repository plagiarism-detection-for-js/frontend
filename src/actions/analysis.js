import { UPDATE_STATUSES_LIST, UPDATE_REPORTS_LIST } from "../constants/analysisConstants"
import { ERROR_ALERT, SUCCESS_ALERT } from "../constants/alertConstants"
import { API_URL } from "../constants/APIConstants"
import { showAlert } from './alert'
import axios from "axios"

// TODO: remove this.
import { statusesData, reportsData } from "./tmp"

export const getReports = async (dispatch, authToken) => {

    await axios
        .get(API_URL + "/document/report", {
            headers: {
                "Authorization": `Bearer ${authToken}`
            },
        })
        .then((response) => {
            if (response.data) {
                dispatch({
                    type: UPDATE_REPORTS_LIST,
                    payload: response.data
                })
                // TODO: Remove this.
                showAlert(dispatch, SUCCESS_ALERT, "Successfully fetched the list of reports")
            }
        })
        .catch((error) => {
            // TODO: Remove this.
            dispatch({
                type: UPDATE_REPORTS_LIST,
                payload: reportsData
            })
            showAlert(dispatch, ERROR_ALERT, "Failed to fetch the list of reports")
        })
}


export const getStatuses = async (dispatch, authToken) => {

    await axios
        .get(API_URL + "/document/analysis", {
            headers: {
                "Authorization": `Bearer ${authToken}`
            },
        })
        .then((response) => {
            if (response.data) {
                dispatch({
                    type: UPDATE_STATUSES_LIST,
                    payload: response.data.slice(0, 20)
                })
                // TODO: Remove this.
                showAlert(dispatch, SUCCESS_ALERT, "Successfully fetched the list of statuses")
            }
        })
        .catch((error) => {
            // TODO: Remove this.
            dispatch({
                type: UPDATE_STATUSES_LIST,
                payload: statusesData
            })
            showAlert(dispatch, ERROR_ALERT, "Failed to fetch the list of statuses")
        })
}


export const sendToAnalysis = async (dispatch, authToken, fileId) => {

    await axios
        .post(API_URL + `/document/start-analysis/${fileId}`, {}, {
            headers: {
                "Authorization": `Bearer ${authToken}`
            },
        })
        .then((response) => {
            showAlert(dispatch, SUCCESS_ALERT, "File analysis has started")
        })
        .catch((error) => {
            showAlert(dispatch, ERROR_ALERT, "Failed to start file analysis")
        })
}

