export const reportsData = [
    {
        "id": 0,
        "analysisId": 0,
        "result": 2,
        "fileId": 0,
        "fileName": "testFile1.js",
        "date": 1618402912097
    },
    {
        "id": 1,
        "analysisId": 1,
        "result": 4,
        "fileId": 1,
        "fileName": "testFile2.js",
        "date": 1618402912097
    },
    {
        "id": 2,
        "analysisId": 2,
        "result": 15,
        "fileId": 2,
        "fileName": "testFile3.js",
        "date": 1618402912097
    },
    {
        "id": 3,
        "analysisId": 3,
        "result": 2,
        "fileId": 3,
        "fileName": "testFile4.js",
        "date": 1618402912097
    },
    {
        "id": 4,
        "analysisId": 4,
        "result": 5,
        "fileId": 4,
        "fileName": "testFile5.js",
        "date": 1618402912097
    },
    {
        "id": 5,
        "analysisId": 5,
        "result": 6,
        "fileId": 5,
        "fileName": "testFile6.js",
        "date": 1618402912097
    },
    {
        "id": 6,
        "analysisId": 6,
        "result": 68,
        "fileId": 6,
        "fileName": "testFile7.js",
        "date": 1618402912097
    },
    {
        "id": 7,
        "analysisId": 7,
        "result": 1,
        "fileId": 7,
        "fileName": "testFile8.js",
        "date": 1618402912097
    },
    {
        "id": 8,
        "analysisId": 8,
        "result": 12,
        "fileId": 8,
        "fileName": "testFile9.js",
        "date": 1618402912097
    },
    {
        "id": 9,
        "analysisId": 9,
        "result": 3,
        "fileId": 9,
        "fileName": "testFile10.js",
        "date": 1618402912097
    },
    {
        "id": 10,
        "analysisId": 10,
        "result": 14,
        "fileId": 10,
        "fileName": "testFile11.js",
        "date": 1618402912097
    },
    {
        "id": 11,
        "analysisId": 11,
        "result": 2,
        "fileId": 11,
        "fileName": "testFile12.js",
        "date": 1618402912097
    },
    {
        "id": 12,
        "analysisId": 12,
        "result": 0,
        "fileId": 12,
        "fileName": "testFile13.js",
        "date": 1618402912097
    },
    {
        "id": 13,
        "analysisId": 13,
        "result": 0,
        "fileId": 13,
        "fileName": "testFile14.js",
        "date": 1618402912097
    },
    {
        "id": 14,
        "analysisId": 14,
        "result": 1,
        "fileId": 14,
        "fileName": "testFile15.js",
        "date": 1618402912097
    },
    {
        "id": 15,
        "analysisId": 15,
        "result": 1,
        "fileId": 15,
        "fileName": "testFile16.js",
        "date": 1618402912097
    },
    {
        "id": 16,
        "analysisId": 16,
        "result": 2,
        "fileId": 16,
        "fileName": "testFile17.js",
        "date": 1618402912097
    },
    {
        "id": 17,
        "analysisId": 17,
        "result": 1,
        "fileId": 17,
        "fileName": "testFile18.js",
        "date": 1618402912097
    },
    {
        "id": 18,
        "analysisId": 18,
        "result": 1,
        "fileId": 18,
        "fileName": "testFile19.js",
        "date": 1618402912097
    },
    {
        "id": 19,
        "analysisId": 19,
        "result": 0,
        "fileId": 19,
        "fileName": "testFile20.js",
        "date": 1618402912097
    }
]


export const statusesData = [
    {
        "analysisId": 1,
        "fileId": "0",
        "fileName": "aaa.js",
        "status": "IN_PROGRESS"
    },
    {
        "analysisId": 2,
        "fileId": "1",
        "fileName": "bbb.js",
        "status": "DONE"
    },
    {
        "analysisId": 3,
        "fileId": "2",
        "fileName": "ccc.js",
        "status": "PENDING"
    },
    {
        "analysisId": 4,
        "fileId": "3",
        "fileName": "aaa.js",
        "status": "IN_PROGRESS"
    },
    {
        "analysisId": 5,
        "fileId": "4",
        "fileName": "bbb.js",
        "status": "DONE"
    },
    {
        "analysisId": 6,
        "fileId": "5",
        "fileName": "ccc.js",
        "status": "PENDING"
    }
]

export const files = [
    {
        "id": 1,
        "fileName": "testFile1.js"
    },
    {
        "id": 2,
        "fileName": "testFile2.js"
    },
    {
        "id": 3,
        "fileName": "testFile3.js"
    },
    {
        "id": 4,
        "fileName": "testFile4.js"
    },
    {
        "id": 5,
        "fileName": "testFile5.js"
    },
    {
        "id": 6,
        "fileName": "testFile6.js"
    }
]

export const templates = [
    {
        "id": 1,
        "fileName": "testFile1.js"
    },
    {
        "id": 2,
        "fileName": "testFile2.js"
    },
    {
        "id": 3,
        "fileName": "testFile3.js"
    },
    {
        "id": 4,
        "fileName": "testFile4.js"
    },
    {
        "id": 5,
        "fileName": "testFile5.js"
    },
    {
        "id": 6,
        "fileName": "testFile6.js"
    }
]