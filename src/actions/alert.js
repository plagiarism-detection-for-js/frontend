import { SET_ALERT, NO_ALERT, NO_MESSAGE } from "../constants/alertConstants"

const setAlert = (dispatch, alert, message) => {
    dispatch({
        type: SET_ALERT,
        payload: {
            alert,
            message
        }
    })
}

export const showAlert = (dispatch, alert, message) => {
    setAlert(dispatch, alert, message)
    setTimeout(() => {
        // Hide alert after 4 seconds
        setAlert(dispatch, NO_ALERT, NO_MESSAGE)
    }, 4000)
}

