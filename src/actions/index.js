import switchRoute from "./route"
import { login, logout, register } from "./auth"
import { showAlert } from "./alert"
import { getStatuses, sendToAnalysis, getReports } from "./analysis"
import { getFiles, uploadFile } from "./file"
import { getTemplateFiles, uploadTemplateFile, markAsTemplate } from "./templateFile"


export { switchRoute, login, logout, register, showAlert, getStatuses, sendToAnalysis, getReports, getFiles, getTemplateFiles, uploadFile, uploadTemplateFile, markAsTemplate }