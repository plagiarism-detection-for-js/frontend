import { ERROR_ALERT, SUCCESS_ALERT } from "../constants/alertConstants"
import { UPDATE_FILES_LIST } from "../constants/fileConstants"
import { API_URL } from "../constants/APIConstants"
import { showAlert } from './alert'
import axios from "axios"

// TODO: remove this.
import { files } from "./tmp"


export const getFiles = async (dispatch, authToken) => {

    await axios
        .get(API_URL + "/download/file", {
            headers: {
                "Authorization": `Bearer ${authToken}`
            },
        })
        .then((response) => {
            if (response.data) {
                dispatch({
                    type: UPDATE_FILES_LIST,
                    payload: response.data
                })
                //TODO: Remove this
                showAlert(dispatch, SUCCESS_ALERT, "Successfully fetched the list of files")
            }
        })
        .catch((error) => {
            // TODO: Remove this.
            dispatch({
                type: UPDATE_FILES_LIST,
                payload: files
            })
            showAlert(dispatch, ERROR_ALERT, "Failed to fetch the list of files")
        })
}


// export const getFileById = async (dispatch, authToken, fileId) => {

//     await axios
//         .get(API_URL + `/download/file/${authToken}`, {
//             headers: {
//                 "Authorization": `Bearer ${authToken}`
//             },
//         })
//         .then((response) => {
//             //TODO: Remove this
//             showAlert(dispatch, SUCCESS_ALERT, "Successfully fetched the list of files")
//         })
//         .catch((error) => {
//             showAlert(dispatch, ERROR_ALERT, "Failed to fetch the list of files")
//         })
// }


export const uploadFile = async (dispatch, authToken, file) => {
    const formData = new FormData()
    formData.append("file", file)

    await axios
        .post(API_URL + "/upload/file", formData, {
            headers: {
                "Content-Type": undefined,
                "Authorization": `Bearer ${authToken}`
            },
        })
        .then((response) => {
            showAlert(dispatch, SUCCESS_ALERT, "The file has been uploaded")
        })
        .catch((error) => {
            showAlert(dispatch, ERROR_ALERT, "Failed to upload the file")
        })
}
