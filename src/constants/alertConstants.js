

export const SET_ALERT = "SET_ALERT"
export const NO_ALERT = "NO_ALERT"
export const NO_MESSAGE = "NO_MESSAGE"


export const SUCCESS_ALERT = "alert alert-success"
export const WARNING_ALERT = "alert alert-warning"
export const ERROR_ALERT = "alert alert-danger"

