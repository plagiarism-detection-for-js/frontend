import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import reportWebVitals from './reportWebVitals'
import { loadState, saveState } from "./localStorage"
import thunk from "redux-thunk"

import { createStore, applyMiddleware, compose } from "redux"
import allReducers from "./reducers"
import { Provider } from "react-redux"


const persistedState = () => {

  const state = loadState()

  if (!state) {
    return {}
  }

  const auth = state.auth
  const routing = state.routing

  return {
    auth,
    routing
  }
}

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const myStore = createStore(
  allReducers,
  persistedState(),
  composeEnhancer(applyMiddleware(thunk))
)

myStore.subscribe(() => {
  saveState({
    routing: myStore.getState().routing,
    auth: myStore.getState().auth
  });
});


ReactDOM.render(
  <Provider store={myStore}>
    <App />,
  </Provider>,
  document.getElementById("root")
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
