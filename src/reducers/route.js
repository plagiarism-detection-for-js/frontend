import { SWITCH_ROUTE, ROUTE_LOGIN } from "../constants/routeConstants"

const initialState = {
    route: ROUTE_LOGIN,
}


const routeReducer = (state = initialState, action) => {
    switch (action.type) {
        case SWITCH_ROUTE:
            return {
                ...state,
                route: action.payload,
            }
        default:
            return state
    }
}

export default routeReducer
