import { combineReducers } from "redux"
import routeReducer from "./route"
import authReducer from "./auth"
import alertReducer from "./alert"
import analysisReducer from "./analysis"
import fileReducer from "./file"

const allReducers = combineReducers({
    routing: routeReducer,
    auth: authReducer,
    alert: alertReducer,
    analysis: analysisReducer,
    file: fileReducer
})

export default allReducers
