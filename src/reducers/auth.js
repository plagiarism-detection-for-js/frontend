import { SIGN_IN, SIGN_OUT, SIGN_UP } from "../constants/authConstants"

const initialState = {
  isSignedIn: false,
  authToken: "",
}


const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case SIGN_IN:
      return {
        ...state,
        isSignedIn: true,
        authToken: action.payload.authToken
      }
    case SIGN_OUT:
      return {
        ...state,
        isSignedIn: false,
        authToken: ""
      }
    case SIGN_UP:
      return {
        ...state,
        isSignedIn: true,
        authToken: action.payload.authToken
      }
    default:
      return state
  }
}

export default authReducer
