import { UPDATE_STATUSES_LIST, UPDATE_REPORTS_LIST } from "../constants/analysisConstants"

const initialState = {
    statusesData: "",
    reportsData: ""
}

const analysisReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_STATUSES_LIST:
            return {
                ...state,
                statusesData: action.payload
            }
        case UPDATE_REPORTS_LIST:
            return {
                ...state,
                reportsData: action.payload
            }
        default:
            return state
    }
}

export default analysisReducer