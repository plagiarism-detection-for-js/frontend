import { NO_ALERT, SET_ALERT } from "../constants/alertConstants"

const initialState = {
    alert: NO_ALERT,
    message: ""
}

const alertReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_ALERT:
            return {
                ...state,
                alert: action.payload.alert,
                message: action.payload.message
            }
        default:
            return state
    }
}

export default alertReducer
