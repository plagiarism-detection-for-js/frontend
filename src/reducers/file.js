import { UPDATE_FILES_LIST, UPDATE_TEMPLATES_LIST } from "../constants/fileConstants"

const initialState = {
    filesData: "",
    templatesData: ""
}

const fileReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_FILES_LIST:
            return {
                ...state,
                filesData: action.payload
            }
        case UPDATE_TEMPLATES_LIST:
            return {
                ...state,
                templatesData: action.payload
            }
        default:
            return state
    }
}

export default fileReducer